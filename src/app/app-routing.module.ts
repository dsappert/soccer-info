import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from "./component/home/home.component";
import {TeamStandingComponent} from "./component/team-standing/team-standing.component";

const routes: Routes = [
  { path: 'home', component: HomeComponent},
  { path: 'team/:id', component: TeamStandingComponent},
  { path: '**', redirectTo: '/home', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
