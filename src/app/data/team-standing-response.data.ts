import {Team} from "./team.data";

export class TeamStandingResponse {
  constructor(
    public results: number,
    public response: TeamStanding
  ) { }
}

export type TeamStanding = Array<SingleTeamStanding>

export class SingleTeamStanding {
  constructor(
    public teams: {
      home: Team,
      away: Team
    },
    public goals: {
      home: number,
      away: number
    },
  ) { }
}
