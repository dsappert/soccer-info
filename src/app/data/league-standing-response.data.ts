import {Team} from "./team.data";

export class LeagueStandingResponse {
  constructor(
    public results: number,
    public response: Array<{
      league: {
        standings: Array<LeagueStanding>
      }
    }>
  ) { }
}

export type LeagueStanding = Array<SingleLeagueStanding>

export class SingleLeagueStanding {
  constructor(
    public rank: string,
    public team: Team,
    public points: number,
    public goalsDiff: number,
    public all: {
      played: number,
      win: number,
      draw: number,
      lose: number
    }
  ) { }
}
