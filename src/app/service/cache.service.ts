import { Injectable } from '@angular/core';
import {LeagueStanding} from "../data/league-standing-response.data";
import {Country} from "../type/country.type";
import {TeamStanding} from "../data/team-standing-response.data";
import {environment} from "../../environment/environment.dev";

@Injectable({
  providedIn: 'root'
})
export class CacheService {

  private leagueStandingKey: string = "LeagueStanding";
  private teamStandingKey: string = "TeamStanding";

  save<T>(key: string, entity: T): void {
    let storageEntity: StorageEntity<T> = new StorageEntity(this.getOffsetDate().getTime(), entity);

    console.log(`Save entity with key ${key} and ttl ${this.dateToString(storageEntity.ttl)}`);
    localStorage.setItem(key, JSON.stringify(storageEntity));
  }

  find<T>(key: string): T | null {
    let result: T | null = null;
    let storageEntity: StorageEntity<T> | null = null;
    let standingString: string | null = localStorage.getItem(key);

    // Check if localStorage holds an object based on the key
    if ((standingString != null) && (standingString.length > 0)) {
      try {
        storageEntity = JSON.parse(standingString) as StorageEntity<T>;
        // Only return stored object if still valid (ttl not expired)
        if (storageEntity.ttl > new Date().getTime()) {
          result = storageEntity.entity;
        } else {
          localStorage.removeItem(key);
          console.log(`Stored entity with key ${key} was invalidated`);
        }
      } catch (e: unknown) {
        console.error(`Could not read localStorage with key ${key}`, e);
      }
    }

    console.log(`Find with key ${key} returns something? ${result != null} : ${this.dateToString(storageEntity?.ttl ?? 0)}`);
    return result;
  }

  saveLeagueStanding(country: Country, standing: LeagueStanding): void {
    return this.save<LeagueStanding>(`${this.leagueStandingKey}${country}`, standing)
  }

  findLeagueStanding(country: Country): LeagueStanding | null {
    return this.find<LeagueStanding>(`${this.leagueStandingKey}${country}`);
  }

  saveTeamStanding(teamId: number, standing: TeamStanding): void {
    return this.save<TeamStanding>(`${this.teamStandingKey}${teamId}`, standing)
  }

  findTeamStanding(teamId: number): TeamStanding | null {
    return this.find<TeamStanding>(`${this.teamStandingKey}${teamId}`);
  }

  getOffsetDate(): Date {
    let now: Date = new Date();
    let offsetTimeMillis: number = now.getTime() + environment.localStorageTtl;
    return new Date(offsetTimeMillis);
  }

  dateToString(date: Date | number): string {
    let newDate: Date = (typeof date === 'number')? new Date(date) : date;
    return newDate.toLocaleString('en-GB', {timeZone: 'UTC'});
  }
}

// Helper class that holds the ttl (time to live) along the actual object
class StorageEntity<T> {
  constructor(
    public ttl: number,
    public entity: T
  ) { }
}
