import { Injectable } from '@angular/core';
import {Country} from "../type/country.type";
import {LeagueStanding, LeagueStandingResponse} from "../data/league-standing-response.data";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment} from "../../environment/environment.dev";
import {filter, map, Observable, of, tap} from "rxjs";
import {TeamStanding, TeamStandingResponse} from "../data/team-standing-response.data";
import {CacheService} from "./cache.service";
import {getLeagueId} from "../type/league.type";

@Injectable({
  providedIn: 'root'
})
export class FootballApiService {

  httpHeaders: HttpHeaders = new HttpHeaders({
    'x-rapidapi-host': environment.soccerApiUrl,
    'x-rapidapi-key': environment.soccerApiKey,
    // CORS related issues
    //'Access-Control-Allow-Origin': '*',
    //'Access-Control-Allow-Headers': 'Content-Type, Authorization, X-Requested-With'
  });

  constructor(
    private http: HttpClient,
    private cacheService: CacheService
  ) { }

  getLeagueStandings(country: Country): Observable<LeagueStanding> {
    // Check if cached response exists
    let cachedLeagueStanding: LeagueStanding | null = this.cacheService.findLeagueStanding(country);
    if (cachedLeagueStanding != null) {
      return of(cachedLeagueStanding);
    }

    // Request data
    let leagueId: number = getLeagueId(country);

    return this.http.get<LeagueStandingResponse>(
      `https://${environment.soccerApiUrl}/standings?league=${leagueId}&season=${this.getCurrentSeason()}`,
      {headers: this.httpHeaders}).pipe(
        filter((rawResponse: LeagueStandingResponse) => {
          return this.hasResults(rawResponse);
        }),
        map((rawResponse: LeagueStandingResponse) => {
          return rawResponse.response[0].league.standings[0];
        }),
        // Store result in cache
        tap((leagueStanding: LeagueStanding) => {
          this.cacheService.saveLeagueStanding(country, leagueStanding);
        })
    );
  }

  getTeamStanding(teamId: number): Observable<TeamStanding> {
    // Check if cached response exists
    let cachedTeamStanding: TeamStanding | null = this.cacheService.findTeamStanding(teamId);
    if (cachedTeamStanding != null) {
      return of(cachedTeamStanding);
    }

    // Request data
    return this.http.get<TeamStandingResponse>(
      `https://${environment.soccerApiUrl}/fixtures?season=${this.getCurrentSeason()}&team=${teamId}&last=10`,
      {headers: this.httpHeaders}).pipe(
      filter((rawResponse: TeamStandingResponse) => {
        return this.hasResults(rawResponse);
      }),
      map((rawResponse: TeamStandingResponse) => {
        return rawResponse.response;
      }),
      // Store result in cache
      tap((teamStanding: TeamStanding) => {
        this.cacheService.saveTeamStanding(teamId, teamStanding);
      })
    );
  }

  getCurrentSeason(): number {
    let now: Date = new Date();
    let currentYear: number = now.getFullYear();
    // March 2024 belongs to season 2023/24, which is decoded as '2023'
    if (now.getMonth() <= 6) {
      currentYear--;
    }
    return currentYear;
  }

  hasResults(standing: LeagueStandingResponse | TeamStandingResponse): boolean {
    if (standing.results <= 0) {
      console.error("Request did not get a proper response");
      return false;
    }
    return true;
  }
}
