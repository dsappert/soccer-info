import { Injectable } from '@angular/core';
import {countries, Country} from "../type/country.type";
import {Observable, ReplaySubject, Subject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class CountryService {

  private currentCountry: Country = countries[0];
  private currentCountrySubject: Subject<Country>;

  // ReplaySubject stores the latest country, initial next call ensures all subscribers will receive at least the default value
  constructor() {
    this.currentCountrySubject = new ReplaySubject<Country>(1);
    this.currentCountrySubject.next(this.currentCountry);
  }

  getAllCountries(): Country[] {
    return countries;
  }

  getCurrentCountry(): Observable<Country> {
    return this.currentCountrySubject.asObservable();
  }

  setCurrentCountry(newCountry: Country): void {
    console.log(`CurrentCountry set to ${newCountry}`)
    this.currentCountry = newCountry;
    this.currentCountrySubject.next(newCountry);
  }
}
