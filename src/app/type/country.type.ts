import {League, leagues} from "./league.type";

export type Country = typeof leagues[number]["country"];

export const countries: Array<string> = leagues.map((league: League) => league.country);
