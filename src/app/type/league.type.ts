import {Country} from "./country.type";

export type League = {
  country: string;
  leagueId: number;
};

export const leagues: Array<League> = [
  {country: 'England', leagueId: 39},
  {country: 'Spain', leagueId: 140},
  {country: 'Germany', leagueId: 78},
  {country: 'France', leagueId: 61},
  {country: 'Italy', leagueId: 135},
] as Array<League>;

export const getLeagueId: Function = (country: Country): number => {
  let league: League | undefined = leagues.find((l: League) => l.country == country);
  return league!.leagueId;
};
