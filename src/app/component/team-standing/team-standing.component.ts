import { Component } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {TeamStanding} from "../../data/team-standing-response.data";
import {FootballApiService} from "../../service/football-api.service";
import {Subscription} from "rxjs";

@Component({
  selector: 'soccer-team-standing',
  templateUrl: './team-standing.component.html',
  styleUrls: ['./team-standing.component.scss']
})
export class TeamStandingComponent {

  protected teamStanding: TeamStanding | null = null;
  private teamStanding$: Subscription | null = null;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private footballApiService: FootballApiService
  ) { }

  ngOnInit(): void {
    let teamId: number | null = this.route.snapshot.params['id'];
    if ((teamId != null) && (teamId >= 0)) {
      this.teamStanding$ = this.footballApiService.getTeamStanding(teamId).subscribe(result => {
        this.teamStanding = result;
      });
    }
  }

  ngOnDestroy(): void {
    this.teamStanding$?.unsubscribe();
  }

  navigateBack(): Promise<boolean> {
    return this.router.navigate(['/home']);
  }
}
