import { Component } from '@angular/core';
import {CountryService} from "../../service/country.service";
import {Country} from "../../type/country.type";
import {LeagueStanding} from "../../data/league-standing-response.data";
import {FootballApiService} from "../../service/football-api.service";
import {Router} from "@angular/router";
import {Subscription} from "rxjs";

@Component({
  selector: 'soccer-league-standing',
  templateUrl: './league-standing.component.html',
  styleUrls: ['./league-standing.component.scss']
})
export class LeagueStandingComponent {

  private currentCountry: Country | null = null;
  private currentCountry$: Subscription;
  protected leagueStanding: LeagueStanding | null = null;

  constructor(
    private countryService: CountryService,
    private footballApiService: FootballApiService,
    private router: Router
  ) {
    this.currentCountry$ = countryService.getCurrentCountry().subscribe((country: Country) => {
      this.currentCountry = country;
      this.footballApiService.getLeagueStandings(country).subscribe(result => {
        this.leagueStanding = result;
      });
    });
  }

  ngOnDestroy(): void {
    this.currentCountry$.unsubscribe();
  }

  openTeamView(teamId: number): Promise<boolean> {
    return this.router.navigate([`/team/${teamId}`])
  }
}
