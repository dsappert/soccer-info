import { Component } from '@angular/core';
import {CountryService} from "../../service/country.service";
import {Country} from "../../type/country.type";

@Component({
  selector: 'soccer-country-selector',
  templateUrl: './country-selector.component.html',
  styleUrls: ['./country-selector.component.scss']
})
export class CountrySelectorComponent {

  constructor(protected countryService: CountryService) {
  }

  selectCountry(country: Country): void {
    this.countryService.setCurrentCountry(country);
  }

}
