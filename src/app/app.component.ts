import { Component } from '@angular/core';

@Component({
  selector: 'soccer-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'soccer-info';
}
