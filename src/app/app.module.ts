import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CountrySelectorComponent } from './component/country-selector/country-selector.component';
import { LeagueStandingComponent } from './component/league-standing/league-standing.component';
import { TeamStandingComponent } from './component/team-standing/team-standing.component';
import { HomeComponent } from './component/home/home.component';
import {HttpClientModule} from "@angular/common/http";

@NgModule({
  declarations: [
    AppComponent,
    CountrySelectorComponent,
    LeagueStandingComponent,
    TeamStandingComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
